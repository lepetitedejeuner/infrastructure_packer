#!/bin/sh -e
# This script installs and configures nginx to listen on port 80 and redirect
# to port 443, and listen on port 443 using the TLS certificat that it finds
# in /etc/letsencrypt
#
# Prerequisites:
# 1. passwordless SSH access to the host is set up
# 2. the TLS certs to already exist on the target machine
# 3. host is Debian based
#

hostname="$1"
if [ "$hostname" = "" ]; then
	echo "Usage: $0 hostname"
	echo ""
	echo "  hostname - hostname of the Proxmox server to modify"
	echo ""
	exit
fi

# Install nginx
ssh root@$hostname "apt-get install -y nginx"

# Set up the nginx configuration file
scp default root@$hostname:
ssh root@$hostname 'sed -i "s/FQDN/`hostname -f`/g" default'
ssh root@$hostname 'if [ -d /etc/letsencrypt/live/`hostname -f` ]; then mv default /etc/nginx/sites-available/default && nginx -t && systemctl restart nginx; fi'
