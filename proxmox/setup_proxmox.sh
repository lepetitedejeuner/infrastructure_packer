#!/bin/sh
# Set up SSH keys, then run this script to set up a proxmox box with FDE
# and ability to SSH in, pre-boot, so you can remotely unlock the FDE
#
# Script is based on the following guide:
# https://tactical-documentation.github.io/post/proxmoxve6-zfs-luks-systemdboot-dropbear/
host="$1"
netmask="$2"
gateway="$3"
interface="$4"
if [ "$host" = "" -o "$netmask" = "" -o "$gateway" = "" ]; then
	echo "Usage: $0 hostname netmask gateway [interface]"
	echo ""
	echo "  hostname - hostname of the Proxmox server to modify"
	echo "  netmask - netmask for Proxmox server (not CIDR format)"
	echo "  gateway - default gateway for Proxmox server"
	echo "  interface - network interface to use for pre-boot environment"
	echo ""
	exit
fi
if [ ! -r ~/.ssh/id_rsa.pub ]; then
	echo "Error: .ssh/id_rsa.pub not found"
	echo "This is required to SSH into the system before fully booting"
	echo "because the version of dropbear in Debian's repos do not yet"
	echo "support ed25519 keys."
	exit
fi

# Default to keeping the second disk
if [ -z $keep_second_disk ]; then
	keep_second_disk=1
fi

wait_for_resilver() {
	echo -n "Waiting for resilvering to get done..."
	ssh root@$host "zpool status | grep 'resilver in progress'"
	until [ $? -eq 1 ]; do
		sleep 1
		echo -n "."
		ssh root@$host "zpool status | grep 'resilver in progress' > /dev/null"
	done
	echo " done"
}

# Get rid of nag screen
ssh root@$host "if [ ! -f /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js.bak ] ; then sed -i.bak \"s/data.status !== 'Active'/false/g\" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service; fi"
ssh root@$host "sed -i.bak \"s/.data.status.toLowerCase() !== 'active'/ && false/g\" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service"
# The developers changed the code to break the patch, this gets the new code
ssh root@$host "sed -i.bak \"s/!== 'active'/== 'crackme'/g\" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service"

# Set up the apt sources
ssh root@$host "rm -f /etc/apt/sources.list.d/pve-enterprise.list"
ssh root@$host 'echo deb http://download.proxmox.com/debian/pve $(cat /etc/os-release | grep CODENAME | sed "s/.*=//") pve-no-subscription > /etc/apt/sources.list.d/pve-community.list'
ssh root@$host "apt-get update"
ssh root@$host "DEBIAN_FRONTEND=noninteractive apt-get upgrade -y" || exit 1

# Get the dependencies we're going to want for FDE and pre-boot SSH
ssh root@$host "apt-get install -y cryptsetup dropbear busybox patch" || exit 1

# Figure out what devices we have in our raid1 array
devices=`ssh root@$host 'zpool status | grep ONLINE | grep -v "state" | grep -vP "[\t ]+rpool" | grep -v mirror-0 | awk "{print \\$1}"'`
echo
echo "DEBUG: devices = $devices"
echo

# detect if these disks are already encrypted or not
is_encrypted=0
for d in $devices; do
	is_encrypted=`ssh root@$host "if [ ! -e /dev/disk/by-id/$d ]; then if [ -e /dev/mapper/$d ]; then echo '1'; else echo 'ERROR'; fi; else echo '0'; fi"`
	echo "DEBUG: $d is_encrypted = $is_encrypted"
	if [ "$is_encrypted" = "ERROR" ]; then
		# No encryped disks, no unencrypted disks...?
		echo "ERROR: Unable to find disks in zpool or /dev/mapper"
		exit 1
	fi
	if [ "$is_encrypted" = "1" ]; then
		# Once we've found one encrypted disk, we know what to do
		echo "Encrypted disk found, skipping encryption step"
		break
	fi
done

if [ "$is_encrypted" = "0" ]; then
	# Standard /bin/sh doesn't support reading in passwords without echoing
	# them to the screen.  We will attempt to do it without the echo first,
	# but if that fails, fall back to a normal read.  The only way the
	# first option will work is if the user ran this script via bash $0 ...
	echo ""
	read -s -p "Enter FDE encryption password you want to use: " password || \
	read -p "Enter FDE encryption password you want to use: " password
	echo ""

	# Remove /etc/crypttab so we can build a new one by appending
	ssh root@$host "rm -f /etc/crypttab"

	# Remove each device, one at a time, and encrypt them
	count=0
	for d in $devices; do
		if [ $count -ne 0 ]; then
			echo "Attaching $previous_device to rpool"
			ssh root@$host "zpool attach rpool $d cryptrpool$count"
			wait_for_resilver
		fi
		count=$((count+1))
		echo "Detaching $d from rpool"
		ssh root@$host "zpool detach rpool $d"

		# If we did one disk, and don't want redundancy, we can break
		if [ $count -gt 1 -a $keep_second_disk -eq 0 ]; then
			break
		fi

		echo $password | ssh root@$host "cryptsetup --batch-mode luksFormat /dev/disk/by-id/$d"
		echo $password | ssh root@$host "cryptsetup luksOpen /dev/disk/by-id/$d cryptrpool$count"
		id=`ssh root@$host "blkid -s PARTUUID -o value /dev/disk/by-id/$d"`
		ssh root@$host "echo \"cryptrpool$count PARTUUID=$id none luks,discard,initramfs\" >> /etc/crypttab"

		previous_device="$d"
	done

	# If we want to keep mirroring in place, reattach the second disk
	if [ $keep_second_disk -ne 0 ]; then
		# Now attach the second encrypted disk to the pool
		ssh root@$host "zpool attach rpool cryptrpool1 cryptrpool2"
		wait_for_resilver
	else
		echo "Wiping ZFS from the disk that is not being put back in the pool: $d"
		ssh root@$host "wipefs -a /dev/disk/by-id/$d"
	fi
fi

# Allow unlocking via dropbear
ssh root@$host "sed -i 's/^BUSYBOX=.*/BUSYBOX=y/g' /etc/initramfs-tools/initramfs.conf"
ssh root@$host "cd /etc/dropbear-initramfs/ ; /usr/lib/dropbear/dropbearconvert dropbear openssh dropbear_rsa_host_key id_rsa ; dropbearkey -y -f dropbear_rsa_host_key | grep '^ssh-rsa ' > id_rsa.pub"
#" #This comment exists just to fix syntax highlighting in vim
# Put your SSH key there (dropbear only supports RSA in Debian 10)
cat ~/.ssh/id_rsa.pub | ssh root@$host "cat - > /etc/dropbear-initramfs/authorized_keys"

# Make sure dropbear starts in initramfs
ssh root@$host "sed -i 's/^NO_START=.*/NO_START=0/g' /etc/default/dropbear"
# Start dropbear on a different port to avoid SSH host key warnings
ssh root@$host "grep ^DROPBEAR_OPTIONS /etc/dropbear-initramfs/config || echo 'DROPBEAR_OPTIONS=\"-p 12345\"' >> /etc/dropbear-initramfs/config"
#'" #This comment exists just to fix syntax highlighting in vim
ssh root@$host "grep ^DROPBEAR_OPTIONS /etc/dropbear-initramfs/config && sed -i 's/DROPBEAR_OPTIONS=.*/DROPBEAR_OPTIONS=\"-p 12345\"/g' /etc/dropbear-initramfs/config"

# Resolve the address of the proxmox server
ipaddress=$(LC_ALL=C nslookup $host 2>/dev/null  | sed -nr '/Name/,+1s|Address(es)?: *||p')
# If using an IP address instead of a hostname, ipaddress will be empty
if [ -z "$ipaddress" ]; then
	ipaddress="$host"
fi
# Update kernel parameters to set up the network and disk decryption
ssh root@$host "echo -n \"root=ZFS=rpool/ROOT/pve-1 boot=zfs ip=$ipaddress::$gateway:$netmask:$host:$interface:none: \" > /etc/kernel/cmdline"
cryptargs=`ssh root@$host "awk '{print \\$2}' /etc/crypttab | sed 's/PART/cryptdevice=/g' | xargs"`
ssh root@$host "echo -n $cryptargs >> /etc/kernel/cmdline"

# Patch a bug in the ZFS startup scripts which causes importing to fail on boot
scp zfs-ifs.patch root@$host:/usr/share/initramfs-tools/scripts/
ssh root@$host "cd /usr/share/initramfs-tools/scripts/ && patch < zfs-ifs.patch && rm zfs-ifs.patch"

# Update initramfs
ssh root@$host "update-initramfs -u -k all"
# Note, these are normal "errors" that do no harm:
#   cryptsetup: ERROR: Couldn't resolve device rpool/ROOT/pve-1
#   cryptsetup: WARNING: Couldn't determine root device
ssh root@$host "pve-efiboot-tool refresh"
