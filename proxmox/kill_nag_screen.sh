#!/bin/sh -e
# This will just unconditionally remove the nag screen.  This is a separate
# script because the nag screen returns each time an upgrade is done.
#
host="$1"
if [ -z "$host" ]; then
	echo "Usage: $0 hostname"
	echo ""
	echo "  hostname - hostname of the Proxmox server to modify"
	echo ""
	exit
fi

# Get rid of nag screen
# Old code
ssh root@$host "sed -i.bak \"s/data.status !== 'Active'/false/g\" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service"
# Newer code
ssh root@$host "sed -i.bak \"s/!== 'active'/== 'crackme'/g\" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service"
