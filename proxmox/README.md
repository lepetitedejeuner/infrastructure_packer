# Proxmox
Proxmox is pretty great, but there are some things in the default installation
that are undesirable.  These include: no encryption, no security updates, and a
nag screen informing you that you don't have a subscription which you don't
need.

# Full disk encryption
Proxmox does not support Full Disk Encryption (FDE) in its installer.  As a
result, the way to get FDE with Proxmox is to do a ZFS installation in RAID0
and then slip encryption in after the installation.  This is done by removing
one of the mirrored disks, encrypting it and then putting it back.

Unfortunately this does require that there be two equally sized disks at the
time of installation.  One of them can be reclaimed after the encryption is
in place.

There is a bug in the latest Debian/Proxmox repos that prevents the ZFS pool
from being imported.  This code path doesn't get hit until the FDE is enabled,
but there is a patch in this directory that will be applied to fix this bug.
The patch file needs to be next to the setup_proxmox.sh script when you run the
script.

## Security updates
The authors of Proxmox do not allow any updates in the default installation,
including but not limited to security patches.  It's important to remove the
enterprise apt repo and add in the free one.  The developers are really
showing contempt for the open source users who do not get a subscription by
setting this default.

## Nag screen
Because the authors of Proxmox do not provide any way to disable the nag screen
that informs you that you don't have a subscription, I've automated the removal
of this screen based on other guides I've seen posted online.  If they allowed
the user to acknowledge that they do not have a subscription and that they do
not want to be shown the nag screen again, I'd consider removing this patch.

## Remote unlock
Whether you are running Proxmox on an old laptop, a workstation, or a proper
rack mount server, it's unlikely that you want to have to go to the server and
type in the FDE password when you reboot.

To address this, the script will install and automatically start busybox and
dropbear early in the boot process, before the root filesystem has been
decrypted.  This allows you to SSH into the server to enter the FDE password so
the machine can continue booting.

After this is set up, SSH into the server on port 12345 and run this:

```
/lib/cryptsetup/askpass "password: " > /lib/cryptsetup/passfifo
```
