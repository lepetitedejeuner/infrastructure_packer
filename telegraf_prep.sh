#!/bin/sh -e
if [ -f influxdb_telegraf_url.txt.pgp ]; then
	rm -f influxdb_telegraf_url.txt
	gpg --decrypt --output influxdb_telegraf_url.txt influxdb_telegraf_url.txt.pgp
fi
if [ -f influxdb_telegraf_password.txt.pgp ]; then
	rm -f influxdb_telegraf_password.txt
	gpg --decrypt --output influxdb_telegraf_password.txt influxdb_telegraf_password.txt.pgp
fi
if [ -f influxdb_telegraf_username.txt.pgp ]; then
	rm -f influxdb_telegraf_username.txt
	gpg --decrypt --output influxdb_telegraf_username.txt influxdb_telegraf_username.txt.pgp
fi
sed -i "s|INFLUXDB_URL|`cat influxdb_telegraf_url.txt`|" telegraf.conf
sed -i "s|INFLUXDB_USERNAME|`cat influxdb_telegraf_username.txt`|" telegraf.conf
sed -i "s|INFLUXDB_PASSWORD|`cat influxdb_telegraf_password.txt`|" telegraf.conf
