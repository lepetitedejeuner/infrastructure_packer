#!/bin/sh -e
# This script deploys the service that gets all patches daily
# to the host that is specified.  It logs in as root to do so
# and expects passwordless login to be set up.
host="$1"

scp upgrade.timer root@$host:/usr/lib/systemd/system/
scp upgrade.service root@$host:/usr/lib/systemd/system/
ssh root@$host "systemctl enable --now upgrade.timer"
