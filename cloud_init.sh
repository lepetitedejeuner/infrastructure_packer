#!/bin/sh -e
export DEBIAN_FRONTEND=noninteractive

# We do not need all this junk
sudo apt-get purge -y update-motd ubuntu-advantage-tools packagekit iw \
	alsa-topology-conf alsa-ucm-conf cpio eatmydata ftp gir1.2-glib-2.0 \
	isc-dhcp-client isc-dhcp-common krb5-locales \
	laptop-detect libeatmydata1 liblmdb0 libmaxminddb0 libplymouth5 \
	libusb-1.0-0 libx11-6 libx11-data libxau6 libxcb1 libxdmcp6 libxext6 \
	libxml2 lshw nano netcat-openbsd ntfs-3g os-prober pci.ids pciutils \
	powermgmt-base publicsuffix telnet usb.ids usbutils wamerican \
	wbritish linux-firmware
sudo apt-get purge -y installation-report || true

# Set up the system so it can be configured via cloud-init
sudo apt-get update
sudo apt-get upgrade -y  # Make sure we're fully patched
sudo apt-get install -y cloud-init auditd audispd-plugins gnupg2 syslog-ng
echo 'datasource_list: [ NoCloud, ConfigDrive ]' | sudo tee /etc/cloud/cloud.cfg.d/99_pve.cfg
# Move any audit rules into place
if [ -f /tmp/settings.rules ]; then
	sudo mv /tmp/settings.rules /etc/audit/rules.d/
fi
if [ -f /tmp/file_operations.rules ]; then
	sudo mv /tmp/file_operations.rules /etc/audit/rules.d/
fi
if [ -f /tmp/process_operations.rules ]; then
	sudo mv /tmp/process_operations.rules /etc/audit/rules.d/
fi
if [ -f /tmp/shell_operations.rules ]; then
	sudo mv /tmp/shell_operations.rules /etc/audit/rules.d/
fi
if [ -f /tmp/file_monitoring.rules ]; then
	sudo mv /tmp/file_monitoring.rules /etc/audit/rules.d/
fi
# And rsyslog configurations
if [ -f /tmp/network.conf -a -d /etc/syslog-ng/conf.d ]; then
	sudo mv /tmp/network.conf /etc/syslog-ng/conf.d/remote.conf
fi
# And if we have a custom resolv.conf, that too
if [ -f /tmp/resolv.conf ]; then
	sudo mv /tmp/resolv.conf /etc/resolv.conf
fi

# Add repo for, and install, telegraf
wget -qO - https://repos.influxdata.com/influxdb.key | sudo apt-key add -
echo "deb https://repos.influxdata.com/debian stretch stable" | sudo tee /etc/apt/sources.list.d/influxdata.list
sudo apt-get update
sudo apt-get install -y telegraf
if [ -f /tmp/telegraf.conf ]; then
	echo "Copying over telegraf configuration"
	sudo cp /tmp/telegraf.conf /etc/telegraf
fi
if [ -f /tmp/rc.local ]; then
	sudo mv /tmp/rc.local /etc/rc.local
fi

# Install our upgrade service, to get ALL patches every day
if [ -f /tmp/upgrade.timer ]; then
	echo "Copying over upgrade timer"
	sudo mv /tmp/upgrade.timer /usr/lib/systemd/system/
fi
if [ -f /tmp/upgrade.service ]; then
	echo "Copying over upgrade service"
	sudo mv /tmp/upgrade.service /usr/lib/systemd/system/
fi
sudo systemctl enable upgrade.timer

# Remove popularity contest
sudo apt-get purge -y popularity-contest

# Only allow SSH logins with SSH keys
echo "PasswordAuthentication no" | sudo tee /etc/ssh/sshd_config.d/no_passwords.conf
echo "PrintMotd no" | sudo tee /etc/ssh/sshd_config.d/no_motd.conf
echo "X11Forwarding no" | sudo tee /etc/ssh/sshd_config.d/no_x11forwarding.conf

# Cloud-init can't handle growing LVM partitions out of the box
# https://forum.proxmox.com/threads/cloud-init-lvm-resize-not-working.68947/
# Also, Debian ships with an old version of growpart that can't handle
# resizing disks due to a bug...
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=868875
# So we will grab the latest to make sure we don't hit that problem
echo "Backing up our version of growpart"
sudo cp /usr/bin/growpart /usr/bin/growpart.orig
echo "Getting latest version of growpart"
sudo wget -O /usr/bin/growpart https://raw.githubusercontent.com/canonical/cloud-utils/master/bin/growpart
sudo chmod +x /usr/bin/growpart  # make sure script is executable

echo "Creating script to resize LVM partitions/volumes/filesystems"
echo '#!/bin/sh' | sudo tee /usr/bin/resizelvm
# Some distros have a swap partition after the main partition, preventing growing
echo 'swapoff -a' | sudo tee -a /usr/bin/resizelvm
echo 'swap=`ls /dev/mapper/| grep swap`' | sudo tee -a /usr/bin/resizelvm
echo 'if [ ! -z "$swap" ]; then' | sudo tee -a /usr/bin/resizelvm
echo "  yes | lvremove /dev/mapper/$swap" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm

echo "if [ -e /dev/sda2 ]; then" | sudo tee -a /usr/bin/resizelvm
echo "  growpart /dev/sda 2" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm
echo "if [ -e /dev/vda2 ]; then" | sudo tee -a /usr/bin/resizelvm
echo "  growpart /dev/vda 2" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm
echo "if [ -e /dev/sda5 ]; then" | sudo tee -a /usr/bin/resizelvm
echo "  growpart /dev/sda 5" | sudo tee -a /usr/bin/resizelvm
echo "  /usr/sbin/pvresize -y -q /dev/sda5" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm
echo "if [ -e /dev/vda5 ]; then" | sudo tee -a /usr/bin/resizelvm
echo "  growpart /dev/vda 5" | sudo tee -a /usr/bin/resizelvm
echo "  /usr/sbin/pvresize -y -q /dev/vda5" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm
echo "if [ -e /dev/vda3 ]; then" | sudo tee -a /usr/bin/resizelvm
echo "  growpart /dev/vda 3" | sudo tee -a /usr/bin/resizelvm
echo "  /usr/sbin/pvresize -y -q /dev/vda3" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm
echo 'name=`ls /dev/mapper/| grep root`' | sudo tee -a /usr/bin/resizelvm
echo 'if [ -z "$name" ]; then' | sudo tee -a /usr/bin/resizelvm
echo '        name=`ls /dev/mapper/| grep -- -lv`' | sudo tee -a /usr/bin/resizelvm
echo 'fi' | sudo tee -a /usr/bin/resizelvm
echo '/usr/sbin/lvresize -y -q -r -l +100%FREE /dev/mapper/$name' | sudo tee -a /usr/bin/resizelvm
echo '/usr/sbin/resize2fs -fFM /dev/mapper/$name || true' | sudo tee -a /usr/bin/resizelvm
sudo chmod +x /usr/bin/resizelvm

# Disable ordered writes on the root filesystem
sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="/GRUB_CMDLINE_LINUX_DEFAULT="rootflags=data=writeback /' /etc/default/grub
sudo update-grub
# Disable the barrier on the root filesystem
sudo sed -i 's/errors=/barrier=0,errors=/' /etc/fstab

echo "Adding bootcmd to resize LVM disk"
echo "bootcmd:" | sudo tee /etc/cloud/cloud.cfg.d/10_resize_disk.cfg
echo "- [ /usr/bin/resizelvm ]" | sudo tee -a /etc/cloud/cloud.cfg.d/10_resize_disk.cfg

# Change some default values
sudo sed -i "s/name: ubuntu/name: vagrant/g" /etc/cloud/cloud.cfg
sudo sed -i "s/name: debian/name: vagrant/g" /etc/cloud/cloud.cfg
sudo sed -i "s/disable_root: true/disable_root: false/g" /etc/cloud/cloud.cfg
sudo sed -i "s/- default/- vagrant/g" /etc/cloud/cloud.cfg
# Delete these packages that we don't want
sudo sed -i "/ - snap/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - ubuntu-advantage/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - disable-ec2-metadata/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - byobu/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - fan/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - landscape/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - lxd/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - puppet/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - chef/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - mcollective/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - salt-minion/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - rightscale_userdata/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - phone-home/d" /etc/cloud/cloud.cfg

# Limit the size of the logs to avoid disk space issues
sudo sed -i "s/#*SystemMaxFileSize=.*/SystemMaxFileSize=300M/" /etc/systemd/journald.conf

# Set up audit logging
sudo sed -i 's/log_format =.*/log_format = ENRICHED/' /etc/audit/auditd.conf
if [ -f /etc/audisp/plugins.d/syslog.conf ]; then
	sudo sed -i 's/active = .*/active = yes/' /etc/audisp/plugins.d/syslog.conf
elif [ -f /etc/audit/plugins.d/syslog.conf ]; then
	sudo sed -i 's/active = .*/active = yes/' /etc/audit/plugins.d/syslog.conf
fi

# Get rid of the insane defaults so we can have nice things
echo "set mouse=
set paste
syntax on" > .vimrc
sudo cp .vimrc /root
sudo cp .vimrc /etc/skel

# If the files that prevent network configuration are there, remove them
if [ -f /var/lib/cloud/data/upgraded-network ]; then
	sudo rm /var/lib/cloud/data/upgraded-network
fi
if [ -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg ]; then
	sudo rm /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg
fi

# Get rid of the insane defaults so we can have nice things
echo "set mouse=
set paste
syntax on" > .vimrc
sudo cp .vimrc /root
sudo cp .vimrc /etc/skel

# Remove unnecessary cron jobs and timers
if [ -f /lib/systemd/system/man-db.timer ]; then
  sudo systemctl disable man-db.timer
fi
if [ -f /etc/cron.daily/man-db ]; then
  sudo rm /etc/cron.daily/man-db
fi
if [ -f /etc/cron.weekly/man-db ]; then
  sudo rm /etc/cron.weekly/man-db
fi
if [ -f /etc/cron.daily/apt-compat ]; then
  sudo rm /etc/cron.daily/apt-compat
fi
if [ -f /etc/cron.daily/bsdmainutils ]; then
  sudo rm /etc/cron.daily/bsdmainutils
fi
if [ -f /lib/systemd/system/motd-news.timer ]; then
  sudo systemctl disable motd-news.timer
fi
if [ -f /lib/systemd/system/unattended-upgrades.service ]; then
  sudo systemctl disable unattended-upgrades.service
fi
sudo update-locale LANG=en_US.UTF-8 || true # Always use UTF-8

# Set timezone and NTP daemon
DEBIAN_FRONTEND=noninteractive TZ=America/New_York apt-get -y install tzdata
apt-get -y install openntpd
if [ -f /tmp/ntpd.conf ]; then
  sudo mv /tmp/ntpd.conf /etc/openntpd/ntpd.conf
fi
sudo systemctl restart openntpd.service

sudo apt-get autoremove -y
