#!/bin/sh -e
# Set up the system so it can be configured via cloud-init
sudo apt-get update
sudo apt-get upgrade -y  # Make sure we're fully patched
sudo apt-get install -y cloud-init
echo 'datasource_list: [ NoCloud, ConfigDrive ]' | sudo tee /etc/cloud/cloud.cfg.d/99_pve.cfg

# Debian ships with an old version of growpart that can't handle
# resizing disks due to a bug...
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=868875
# So we will grab the latest to make sure we don't hit that problem
cp /usr/bin/growpart /usr/bin/growpart.orig
wget -O /usr/bin/growpart https://raw.githubusercontent.com/canonical/cloud-utils/master/bin/growpart
chmod +x /usr/bin/growpart

# Cloud-init can't handle growing LVM partitions out of the box
# https://forum.proxmox.com/threads/cloud-init-lvm-resize-not-working.68947/
echo '#!/bin/sh' | sudo tee /usr/bin/resizelvm
# Some distros have a swap partition after the main partition, preventing growing
echo 'swapoff -a' | sudo tee -a /usr/bin/resizelvm
echo 'swap=`ls /dev/mapper/| grep swap`' | sudo tee -a /usr/bin/resizelvm
echo 'if [ ! -z "$swap" ]; then' | sudo tee -a /usr/bin/resizelvm
echo "  yes | lvremove /dev/mapper/$swap" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm

echo "if [ -e /dev/sda2 ]; then" | sudo tee -a /usr/bin/resizelvm
echo "  growpart /dev/sda 2" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm
echo "if [ -e /dev/sda5 ]; then" | sudo tee -a /usr/bin/resizelvm
echo "  growpart /dev/sda 5" | sudo tee -a /usr/bin/resizelvm
echo "  /usr/sbin/pvresize -y -q /dev/sda5" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm
echo "if [ -e /dev/vda3 ]; then" | sudo tee -a /usr/bin/resizelvm
echo "  growpart /dev/vda 3" | sudo tee -a /usr/bin/resizelvm
echo "  /usr/sbin/pvresize -y -q /dev/vda3" | sudo tee -a /usr/bin/resizelvm
echo "fi" | sudo tee -a /usr/bin/resizelvm

echo 'name=`ls /dev/mapper/| grep root`' | sudo tee -a /usr/bin/resizelvm
echo 'lvresize -y -q -r -l +100%FREE /dev/mapper/$name' | sudo tee -a /usr/bin/resizelvm
echo 'resize2fs -fFM /dev/mapper/$name' | sudo tee -a /usr/bin/resizelvm
sudo chmod +x /usr/bin/resizelvm

echo "bootcmd:" | sudo tee /etc/cloud/cloud.cfg.d/10_resize_disk.cfg
echo "- [ /usr/bin/resizelvm ]" | sudo tee -a /etc/cloud/cloud.cfg.d/10_resize_disk.cfg

# Change some default values
sudo sed -i "s/name: debian/name: vagrant/g" /etc/cloud/cloud.cfg
sudo sed -i "s/disable_root: true/disable_root: false/g" /etc/cloud/cloud.cfg
sudo sed -i "s/- default/- vagrant/g" /etc/cloud/cloud.cfg
# Delete these packages that we don't want
sudo sed -i "/ - snap/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - disable-ec2-metadata/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - byobu/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - fan/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - landscape/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - lxd/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - puppet/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - chef/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - mcollective/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - salt-minion/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - rightscale_userdata/d" /etc/cloud/cloud.cfg
sudo sed -i "/ - phone-home/d" /etc/cloud/cloud.cfg
