# Prerequisites
* [Packer](https://www.packer.io/)
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
* [Proxmox](https://proxmox.com/en/downloads) server of your own

# Usage
First, you will need to set some environment variables:
```
export PROXMOX_URL="https://thyme.example.com:8006/api2/json"
export PROXMOX_USERNAME="root@pam"
export PROXMOX_PASSWORD="hunter2"
```

Obviously the values need to be whatever you are using for your system.  It'd
be best to not do everything as root, but root@pam is the only user in a
default Proxmox install, hence it is used in our example.

The other thing that will need to be changed is the name of the Proxmox node
specified in fedora.json.  This is the "node" entry, whose default value is
"thyme".

## Fedora
There will be a few changes you will likely need to make in order to build
Fedora on your network.  Fedora uses kickstart, which boots the installer and
wants to fetch a kickstart configuration file from the machine where you are
running Packer.  This means that the installer needs to be configured with an
IP address.

The IP address is passed in as a kernel parameter.  This can be seen in the
"boot_command" section of fedora.json.  The format is
VM_IP::GATEWAY:NETMASK::NIC_DEVICE:TYPE where TYPE is "none" for a static IP.
If you have a DHCP server on your network, you can omit the ip= and nameserver=
parameters from the boot command.  Remember that this is the IP address of the
installer, not the configured VM.  These command line options are [documented
by
RedHat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-configuring_ip_networking_from_the_kernel_command_line).

The other change you'll probably want to make is the IP address that the VM
should use when it is configured.  This is set by the kickstart configuration
file (config/ks.cfg).  Take a look at the line that starts with "network" and
the arguments should be pretty self explanitory.  When you change this, you
will also need to change the "ssh_host" in fedora.json.  This variables tells
packer where to watch for the SSH server to be started so it can run the
provisioners.

Now we are finally ready to build a Fedora VM.  Here's how to build Fedora 30:
```
packer build -var-file fedora30.json fedora.json
```

Logging into Proxmox's web interface will allow you to see the VM boot up, the
installer run, the reboot into the installed OS, and the provisioning that is
done over SSH (by ansible).  This is valuable in debugging when things do not
go as expected.

Similarly, here's how to build Ubuntu 18.04 or 20.04:
```
packer build -var-file=ubuntu1804.json ubuntu.json
packer build -var-file=ubuntu2004.json ubuntu.json
```

# Customization
The packer files include an Ansible provisioner so you can see how to set up
specific packages in your base VM images.  The example installs a web server,
which is probably not what you want in your base image.  To remove it, you can
just remove the "provisioners" section in the JSON file that is given to packer.

# Troubleshooting
Q. Fedora 30 gives an "Unable to find a match" error related to the command:
`dnf install -y python2-dnf`.  What's wrong?
A. This is an [issue](https://github.com/ansible/ansible/issues/54855) and a
workaround was added in Ansible 2.8.  The workaround from the ticket has been
applied to the httpd.yml playbook, so if you get this error, something has gone
quite wrong.  For more info, see the [Python Interpreter
Discovery](https://docs.ansible.com/ansible/devel/porting_guides/porting_guide_2.8.html#python-interpreter-discovery)
page.
