variable "SSH_KEY" {
  default = "#INSERTSSHHPUBLICKEYHERE"
}
variable "TARGET_NODE" {
}
variable "hostname" {
  default = "ns1"
}
variable "ip_address" {
  default = "192.168.1.208"
}
variable "cidr" {
  default = "24"
}
variable "gateway" {
  default = "192.168.1.1"
}

terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
    }
  }
}

provider "proxmox" {
}

resource "proxmox_vm_qemu" "proxmox_vm" {
  count             = 1
  name              = var.hostname
  target_node       = var.TARGET_NODE
  clone             = "ubuntu-18.04"
  os_type           = "cloud-init"
  cores             = 1
  sockets           = "1"
  cpu               = "host"
  memory            = 512
  scsihw            = "virtio-scsi-pci"
  bootdisk          = "scsi0"
  disk {
    size            = "20G"
    type            = "scsi"
    storage         = "zfsdisk"
    iothread        = true
  }
  network {
    model           = "virtio"
    bridge          = "vmbr0"
  }
  # Cloud Init Settings
  ipconfig0 = "ip=${var.ip_address}/${var.cidr},gw=${var.gateway}"
  sshkeys = <<EOF
  ${var.SSH_KEY}
  EOF
}

