# Dependencies
Requires Terraform version 0.12 or later (API version 5), and a proxmox
provider.

## Terraform
The instructions below work for installing terraform from an AMD64 Ubuntu
machine.

```
# Install terraform
sudo apt-get install -y software-properties-common
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install -y terraform
```

## Proxmox plugin
The proxmox plugin for Terraform has been published in the official registry.
If this is not the case on your operating system or CPU architecture, check out
the releases page:

https://github.com/Telmate/terraform-provider-proxmox/releases/

If you get it from GitHub, you'll need to create
./terraform.d/plugins/linux_amd64/ and put the plugin executable in that
directory.

# Setup
The terraform scripts require variables to be set that contain the username and
password to log into Proxmox, along with the URL of the Proxmox server.  If you
already have these set up in PROXMOX_USERNAME, PROXMOX_PASSWORD, and
PROXMOX_URL, then all you need to do is this:

```
export PM_API_URL="$PROXMOX_URL"
export PM_USER="$PROXMOX_USERNAME"
export PM_PASS="$PROXMOX_PASSWORD"
export PM_TLS_INSECURE="true"
```

# Security
Setting sensitive information on the command line (using the
`-var-file="name=value"` option) is not recommended, as it will expose this
information to everyone else on the machine (via the process list).

Similarly, setting the variables with hardcoded strings will put the plaintext
password in your shell history.  Depending on your shell and its options, this
may be worked around by prepending the command with a leading space.  Make sure
you test this before using it to enter your password.

# Review the config
Once the required variables are set, you can run `terraform plan` with the
required variables specificed on the command line.

Example:
```
terraform plan -var="TARGET_NODE=thyme" -var="hostname=ns1.example.org" -var="ip_address=192.168.1.208" -var="gateway=192.168.1.1" -var="SSH_KEY=ssh-ed25519 AAAAC3isjflk23i3nxfhAAAAfd8dNDe3rdfnd/d8dfDFd8fdfhnDfndfshdfsDfdfhsZ" -out=the_plan
```

# Deploy the VMs
If you were happy with your config, you can now deploy with `terraform apply`
with the name of your plan file (`the_plan` in the example above).

```
terraform apply the_plan
```

